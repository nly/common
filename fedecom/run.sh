#!/bin/sh

cd web-client || exit

bld='git pull;  npm install;  npm run build'

nix-shell -p git nodejs\
	--command "${1:+${bld}; } & \$(cat serve)"

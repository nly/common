Adding products using curl
--------
Since I am just working with Http and Json. It makes things simple and
I don't require a graphical client. I'll try listing Mangoes on the
shopping site.

First off I need to install curl command.
$ nix-env -iA nixos.curl

I can test if it works like so:
$ curl https://example.com

I see a lot of text that looks like Html so it's working :)

I just ran curl with no flags, and by design it does a Get request on
the url. But, I need to post some data to a url so i need these flags:
$ curl --request POST --data ''

I also need to tell the server that i'm sending Json data and not
something arbitrary:
$ ... --header "Content-Type: application/json"

And, the url is https://xopow.cyka.cf. So the final query looks like
this:

curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"id":"234bhdutest01", "name":"Mango", "stock":10, "price":1, "shortDesc":"Delicious indigenous fruit of India", "description":"Yellow, pulpy fruit of India"}' \
  https://xopow.cyka.cf/products

About
--------
This is a dump of ideas which seem worthwhile to work on. At first
every idea appears to be a novel idea, and even a good idea. As the
available options change, or knowledge of available options increases
the ideas will be classified under one of the following:

#open – No experimental evidence to suggest with confidence that it
 works or not.

#closed – A dead end. Does not work.

#solved – A working product exists, exactly or closely matching the
 requirements.

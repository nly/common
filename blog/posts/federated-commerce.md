Federated E-commerce
--------
End violence with free trade. There is fighting over who owns what. No
argument can make someone else give up what you want. There will be
conflict. The only path forward is to make it easy to trade, so that
people have motivation to give away their wealth, maybe not for free,
but in exchange for something else. E-commerce could be as simple as
using social media. Physical businesses being ripped out by
regulations will serve as the fertile ground where online and smaller
businesses can thrive. Whoever builds an open source, platform for
e-commerce should probably use Federation to link up stores into
store-chains.

A world where everybody is a business owner would be awesome. You
could have your own shop, if you want. Trade should be so easy that
you could live in a house for a few months, sell it and move to
somewhere else in the winter.

#open

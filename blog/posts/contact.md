Contact
--------
CEO of Capitalism. This blog is written by me.

Prosperity arises as a result of voluntary exchange of products of specialised efforts 👍

Email: nly@disroot.org
Mastodon: https://lor.sh/@nly
Git: https://codeberg.org/nly
Federated E-commerce: https://shop.play.ai
Matrix: @mly:disencord.disenthrall.me
GNU Jami: opn

Monero 83y8tUMdD7fBeyXDdTm1Mu2ufKTMJJWRQWQAT3Qxwnm3FtSSvhgkPcF1ByYxEsLMgXUHcRfnZnEzo6ZyaZcDdvoiHAFitSF

A new type of operating system
--------
Computer boots up, but keyboard input, network inputs, or anything
else does not grant you any access. The computer could use a display
to instruct you to send some 'token' to begin using facilities like
the keyboard. If it's your personal computer then you can figure out
the private key to the wallet, and keep reusing the same token to keep
using the computer infinitely. The program binary, source, and all
other data could be stored encrypted to prevent breaking in. The
initial program may startup and boot another program with newly
generated keys and switch to it and re-encrypt everything to the new
key. The services running on the computer may have a seperate process
each, with each their own encryption and keys. The master process can
control their wallets and hence the behaviour of these services. These
services too can be incentivized to provide access their facilities,
again the master process will have the job of accumulating all the
'tokens' from these processes and gathering them in it's own
wallet. The owner of the computer controls the root wallet and can
re-imburse the tokens to him/her-self after every use. Outside agents
do not control the wallets, so they will have to pay a price to access
the services, detering abuse. The prices can be set exorbitantly high
to prevent any use of the service by any external agency, but the user
can just re-imburse himself(provided enough tokens exist in the first
place). Another option would be to make the computer consider the
user's own tokens as 'more valuable' than any other, or even not
consider any other tokens. If some service is being over-exerted by
requests but it cannot generate enough 'tokens' to sustain itself, the
master process/kernel would stop considering it's calls important, and
schedule it out to pause until it regains it's 'tokens' to resume
operation. In all of this the user can still keep let's say the 'http'
service running even if it's operating at a 'loss' by re-allocating
assets from master/kernel wallet to the 'http' wallet. In this manner
the user can have complete autonomy over the system, while the system
has an illusion of full autonomy over itself.

Machine Economy
--------

Not really. But I want a responsible computer butler.

It's safe to assume that public data is preserved forever. Once it's
on the internet, then it's out there. But private data gets lost all
the time? lost passwords, lost some file, machines dying or getting
stolen. I should clarify what i mean by public data, data revealed to
the public so now somebody else could have a copy of it. It's not
their responsibility to keep it publicly available, it's just that now
they have a personal copy too. And so, if the given information was
worth anything to them they have an incentive to preserve it, one
man's trash is another's treasure. And ofcourse they could pass it on
to somebody else and so on.

But there is very important private data too. Things like passwords
act as a key that then unlocks other assets, like more data but also
things like money in a bank account, maybe the bicycle lock and
perhaps even the front door. So, privacy is a serious matter, I can
lose actual scarce resource if I'm not careful. There are many
solutions to the problem, keeping an offline datastore helps keep the
information away from attackers even in the case I were
hacked. Encryption makes it so that even if the attacker gets hands on
my sensitive data they could not decipher it before I and my
grandchildren are long gone. I could lose some private information
like addresses and such, which could then threaten me or expose me to
further attacks on me and my belongings.

There is a very interesting outcome of everyday computers. Nobody
thought possible that somebody could own anybody else's brain. Sure,
It's possible to own another person's body err... labour and so
on. But that's not quite like ownership, only as long as the other
person is willing to bow down to your whims. Certainly, you cannot own
somebody else's brain. But with computers I can have decision making,
and logic and reasoning outside of my own brain. I can have eyes, sort
of, and ears and such. But computers do not really have a framework
for working with other computers, yet.

Computers, and computer networks work in a very primitive
fashion. Nothing like humans working together. Because, I submit, that
they don't have a sense of ownership and responsibility. A computer
doesn't have a filter, if it's a server and it's task is to give away
information, it doesn't care that it's leaking private information
about me and itself that will lead to it's own destruction. Computers
don't have a framework for ownership of data. But, I'm also interested
that my computer be able to own other assets, on my behalf. Only so
that I can free myself from taking care of them. Cryptocurrencies that
do smart contracts are interesting. And to be super secure, I can
create a personal wallet, that even the computer has no access to, and
program it to transfer everything to my account every so often.

The other question is how should my computer deal with other
computers? It should preserve my private information, it should try to
act in a responsible manner to what goal? It should be just like how
the economy works, the peaceful engine of prosperity. Prices would be
the ideal candidate. They serve the purpose of picking the most
favorable option out of a given choices. I think machine learning
algorithms already have these, but they call them something else
instead of a price, weights or incentive maybe. And one crucial
difference between humans and computers is that computers can own
other computers, there is no computer rights issue about that.

So my personal computerised butler could span many physical machines,
and still be one single entity, with one unified goal of serving me to
the best of it's abilities. Or maybe not, maybe it's better to have
seperate machines but each with the goal of serving me. Need to solve
for ownership and trade, ownership of things like data, programs,
computing resources but also real assets, like money and more.
